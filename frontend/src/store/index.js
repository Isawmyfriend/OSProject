import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    orderList: []
  },

  mutations: {
    updateOrder(state, product) {

    console.log(product.product_id)
    const index = state.orderList.findIndex((e) => e.id == product.product_id);

      if (index == -1) {
        state.orderList.push({
          id: product.product_id,
          name: product.product_name,
          pricePerUnit: product.product_price,
          quantity: 1,
        });
      } else {
        state.orderList[index].quantity++;
      }

      console.log(state.orderList)

    },

    removeOrder(state, id) {
      const index = state.orderList.findIndex((e) => e.id == id);

      if (index != -1) {
        state.orderList.splice(index, 1)
      }
    },

    OrderListLength(state) {
       return state.orderList.length
    },

    clearOrderList(state) {
      state.orderList = []
    }
  },

  actions: {
    addOrder({ commit }, product) {
      commit('updateOrder', product)
    },

    removeOrder({ commit }, id) {
      commit('removeOrder', id)
    }
  },

  modules: {
  }
})
