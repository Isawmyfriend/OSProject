package main

import (

	"product/model"
	_ "fmt"
	"net/http"
	"github.com/rs/cors"
	"github.com/gorilla/mux"
)

func main() {

	router := mux.NewRouter()

	//Insert
	router.HandleFunc("/product/insertproduct", model.InsertProduct).Methods("POST")

	//GET
	router.HandleFunc("/product", model.ReadAllProduct).Methods("GET")

	handler := cors.Default().Handler(router)

	http.ListenAndServe(":85", handler)

}
