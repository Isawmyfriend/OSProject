package model

import (
	"context"
	"encoding/json"
	_"fmt"
	"log"
	"net/http"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"
)


type Product struct {
	
	ProductID       uint64 `json:"product_id"`
	ProductName     string `json:"product_name"`
	ProductPrice    float64 `json:"product_price"`
	Productsrc   string `json:"product_src"`
}



func InsertProduct(res http.ResponseWriter,req *http.Request) {

	res.Header().Set("Content-Type", "text/html; charset=ascii")
	res.Header().Set("Access-Control-Allow-Origin", "*")
	res.Header().Set("Access-Control-Allow-Headers","Content-Type,access-control-allow-origin, access-control-allow-headers")

	client, _ := mongo.NewClient(options.Client().ApplyURI(
		"mongodb+srv://NER0:<password>@cluster0.krxvu.mongodb.net/myFirstDatabase?retryWrites=true&w=majority",
	))

	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
	err := client.Connect(ctx)

	defer client.Disconnect(ctx)
	err = client.Ping(ctx, readpref.Primary())
	if err != nil {
		log.Fatal(err)
	}

	var product Product
	osProductDatabase := client.Database("OS_Product")
	productCollection := osProductDatabase.Collection("product")

	json.NewDecoder(req.Body).Decode(&product)
	productResult, err := productCollection.InsertOne(ctx, product)

	if err != nil {
		log.Fatal(err)
	}

	json.NewEncoder(res).Encode(productResult)

}

func  ReadAllProduct(res http.ResponseWriter,req *http.Request) {

	res.Header().Set("Content-Type", "text/html; charset=ascii")
	res.Header().Set("Access-Control-Allow-Origin", "*")
	res.Header().Set("Access-Control-Allow-Headers","Content-Type,access-control-allow-origin, access-control-allow-headers")

	client, _ := mongo.NewClient(options.Client().ApplyURI(
		"mongodb+srv://NER0:<password>@cluster0.krxvu.mongodb.net/myFirstDatabase?retryWrites=true&w=majority",
	))

	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
	err := client.Connect(ctx)

	defer client.Disconnect(ctx)
	err = client.Ping(ctx, readpref.Primary())
	if err != nil {
		log.Fatal(err)
	}

	var products []Product

	osProductDatabase := client.Database("OS_Product")
	productCollection := osProductDatabase.Collection("product")

	cursor, err := productCollection.Find(ctx, bson.M{})
	if err != nil {
		log.Fatal(err)
	}

	defer cursor.Close(ctx)

	for cursor.Next(ctx) {

		var p Product
		if err = cursor.Decode(&p); err != nil {
			log.Fatal(err)
		}
		products = append(products, p)

	}

	json.NewEncoder(res).Encode(products)


}
