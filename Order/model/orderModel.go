package model

import (
	"context"
	"encoding/json"
	_"fmt"
	"log"
	"net/http"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"
)


type Order struct {
	
	OrderID    uint64 `json:"order_id"`
	OrderName     string `json:"order_name"`
	OrderPrice    float64 `json:"order_price"`
	
}

type Orders struct {
    Orderslist []Order `json:"array"`
}







func InsertOrder(res http.ResponseWriter,req *http.Request) {

	res.Header().Set("Content-Type", "text/html; charset=ascii")
	res.Header().Set("Access-Control-Allow-Origin", "*")
	res.Header().Set("Access-Control-Allow-Headers","Content-Type,access-control-allow-origin, access-control-allow-headers")

	client, _ := mongo.NewClient(options.Client().ApplyURI(
		"mongodb+srv://NER0:<password>@cluster0.krxvu.mongodb.net/myFirstDatabase?retryWrites=true&w=majority",
	))

	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
	err := client.Connect(ctx)

	defer client.Disconnect(ctx)
	err = client.Ping(ctx, readpref.Primary())
	if err != nil {
		log.Fatal(err)
	}

	var order Orders
	osOrderDatabase := client.Database("OS_Product")
	orderCollection := osOrderDatabase.Collection("order")

	json.NewDecoder(req.Body).Decode(&order)

	orderResult, err := orderCollection.InsertOne(ctx, order)

	if err != nil {
		log.Fatal(err)
	}

	json.NewEncoder(res).Encode(orderResult)

}

func  ReadAllOrder(res http.ResponseWriter,req *http.Request) {

	res.Header().Set("Content-Type", "text/html; charset=ascii")
	res.Header().Set("Access-Control-Allow-Origin", "*")
	res.Header().Set("Access-Control-Allow-Headers","Content-Type,access-control-allow-origin, access-control-allow-headers")


	client, _ := mongo.NewClient(options.Client().ApplyURI(
		"mongodb+srv://NER0:<password>@cluster0.krxvu.mongodb.net/myFirstDatabase?retryWrites=true&w=majority",
	))

	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
	err := client.Connect(ctx)

	defer client.Disconnect(ctx)
	err = client.Ping(ctx, readpref.Primary())
	if err != nil {
		log.Fatal(err)
	}


	var orders []Orders

	osOrderDatabase := client.Database("OS_Product")
	orderCollection := osOrderDatabase.Collection("order")

	cursor, err := orderCollection.Find(ctx, bson.M{})
	if err != nil {
		log.Fatal(err)
	}

	defer cursor.Close(ctx)

	for cursor.Next(ctx) {

		var p Orders
		if err = cursor.Decode(&p); err != nil {
			log.Fatal(err)
		}
		orders = append(orders, p)

	}

	json.NewEncoder(res).Encode(orders)


}
