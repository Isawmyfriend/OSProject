module order

go 1.15

require (
	github.com/gorilla/mux v1.8.0 // indirect
	github.com/rs/cors v1.7.0 // indirect
	go.mongodb.org/mongo-driver v1.5.1
)
